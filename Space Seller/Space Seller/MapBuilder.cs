﻿using System;
using System.Collections.Generic;



public static class MapBuilder
{
    //Resource table
    //Se hace copia del mapa aquí?
    private static List<Map> map_list = null;
    //Numero de transaccion y la transaccion en si.
    private static Dictionary<string, Transaction> transaction_list = new Dictionary<string, Transaction>();

    public static void addTransactionToGlobList(KeyValuePair<string, Transaction> t)
    {
        if (transaction_list.ContainsKey(t.Key))
        {
            int c = 0;
            string new_string; 
            do
            {
                new_string = t.Key + c.ToString();
                ++c;
            } while (transaction_list.ContainsKey(new_string));
        }
        transaction_list.Add(t.Key, t.Value);
    }

    public static void addTransactionToGlobList(Transaction t)
    {
        string s = transaction_list.Count.ToString();
        transaction_list.Add(s, t);
    }

    public static Dictionary<string, Transaction> getTransactionsGlobList()
    {
        return transaction_list;
    }

    public static Map buildMap(int number_planets = 10, string id = "Unnamed Map.")
    {
        //?¿?¿
        var rt = ResourceBuilder.buildResourcesTables();
        MapBuilder.Map map = new MapBuilder.Map(rt, number_planets, id);
        createRoutes(map);
        map_list = new List<Map>();
        MapBuilder.map_list.Add(map);
        map.setPlanetsResources();
        return map;
    }

    public static ref List<Map> getMapList()
    {
        return ref map_list;
    }

    private static void createRoutes(Map map)
    {
        var planets_list = map.getPlanets();
        int routes_created = 0;
        Random rnd = new Random();
        int planet_to_comunicate;

        for (int i = 0; i < planets_list.Count; ++i)
        {
            planet_to_comunicate = rnd.Next(0, planets_list.Count);
            if (planets_list[planet_to_comunicate].has_routes)
            {
                do
                {
                    planet_to_comunicate = rnd.Next(0, planets_list.Count);
                } while (!planets_list[planet_to_comunicate].has_routes);
                RouteBuilder.Route route = new RouteBuilder.Route(planets_list[i], planets_list[planet_to_comunicate]);
                ++routes_created;
            }
            else
            {
                RouteBuilder.Route route = new RouteBuilder.Route(planets_list[i], planets_list[planet_to_comunicate]);
                ++routes_created; 
            }

        }
        Console.WriteLine("");
        Console.WriteLine("Routes createad: " + routes_created);
        Console.WriteLine("");
    }

    public class Map
    {
        private string id = "Unnamed Map.";
        private int resources_table_size = 0;

        private Dictionary<string, int> resources_table = null;
        private List<ResourceBuilder.Resource> resources_list = null; 

        private List<PlanetBuilder.Planet> planet_list;

        public Map(Dictionary <string, int> resources_table, int number_planets = 10, string id = "Unnamed Map." )
        {
            planet_list = new List<PlanetBuilder.Planet>(number_planets);
            this.setResourcesTables(resources_table); 
            createPlanets(number_planets); 

        }

        public void setId(string id)
        {
            this.id = id;
        }

        public void setResourcesTables(Dictionary<string, int> resources_tables_)
        {
            this.resources_table = resources_tables_;
            this.resources_table_size = this.resources_table.Count;
        }

        public void setResourcesList(List<ResourceBuilder.Resource> resources)
        {
            this.resources_list = resources; 
        }

        public void setPlanetsResources()
        {
            foreach (var planet in this.planet_list)
            {
                planet.setResourcesMultipliers();
            }
        }

        public string getId()
        {
            return this.id; 
        }

        public int getResourcesTableSize()
        {
            return this.resources_table.Count; 
        }

        public List<PlanetBuilder.Planet> getPlanets()
        {
            return this.planet_list; 
        }

        public Dictionary<string, int> getResourcesTable()
        {
            return this.resources_table;
        }

        private void createPlanets(int number_planets)
        {
            for (int i = 0; i < number_planets; ++i)
            {
                PlanetBuilder.Planet planet = new PlanetBuilder.Planet(("Planet" + i));
                planet.setPlanetIndex(i);
                planet_list.Add(planet);

            }
        }
    }
}