﻿using System;
using System.Collections.Generic;


public class Cargo
{
    private int total_capacity = 100;
    private int actual_capacity = 0;
    private bool manifest_initialized = false;
    private bool manifest_filled = false; 
    private Dictionary<string, int> cargo_manifest = null;


    public Cargo(int total_capacity)
    {
        this.total_capacity = total_capacity;
        this.actual_capacity = total_capacity;
        this.initCargoManifest();
        this.setRandomCargoManifest();
    }

    public int getTotalCapacity()
    {
        return this.total_capacity;
    }

    public int getMaximumCapacity()
    {
        return this.actual_capacity;
    }

    public int getRealCapacity()
    {
        int total_space = 0;
        foreach (var val in this.cargo_manifest)
        {
            total_space += val.Value;
        }
        return total_space;
    }

    public void setActualCapacity(int actual_capacity)
    {
        this.checkCapacity();
        this.actual_capacity = actual_capacity;
        this.checkCapacity();
    }

    public void setTotalCapacity(int total_capacity)
    {
        this.checkCapacity();
        this.total_capacity = total_capacity;
    }

    public void setRandomCargoManifest()
    {
        Random rnd = new Random();
        int remaining_space = this.actual_capacity;
        int count = 0;
        foreach (var good in cargo_manifest)
        {
            ++count;
           
            int actual_val = rnd.Next(1, remaining_space);
            
            this.setManifestAmount(good.Key, actual_val);
            remaining_space -= actual_val;
         
        }
        int division = remaining_space / count;
        this.printManifestTable();
        foreach (var m in this.cargo_manifest)
        {
            this.addManifestAmount(m.Key, division);
        }
        this.printManifestTable();
        if (this.getRealCapacity() > this.getMaximumCapacity())
        {
            int difference = this.getMaximumCapacity() - this.getRealCapacity();
            this.addManifestAmount("Fuel", -(difference));
        }

        else if (this.getRealCapacity() < this.getMaximumCapacity())
        {
            int difference = this.getMaximumCapacity() - this.getRealCapacity();
            this.addManifestAmount("Fuel", (difference));
        }

        this.manifest_filled = true;
    }

    public void printManifestTable()
    {
        foreach (var entry in cargo_manifest)
        {
            Console.WriteLine("Good: " + entry.Key + " Amount: " + entry.Value);
        }
    }

    //Se cambian los valores uno a uno haciendo copias del diccionario. Esta operacion no es tan cara, ya que 
    //no habra mas de diez recursos en la tabla global de recursos.
    public void addManifestAmount(string key, int sumed_value)
    {
        int ind = this.cargo_manifest[key];
        int old_value;
        int new_value;
        if (this.cargo_manifest.TryGetValue(key, out ind))
        {
            old_value = this.cargo_manifest[key];
            new_value = old_value + sumed_value;
            this.setManifestAmount(key, new_value);
        }

        else
            throw new Exception("Error trying to add an amount to the manifest.");

    }

    public void substractManifestAmount(string key, int sub_value)
    {
        this.addManifestAmount(key, -sub_value);
    }

    public bool setManifestAmount(string key, int new_value)
    {
        Dictionary<string, int> temp_dic = new Dictionary<string, int>();
        int index = 0;
        foreach (var val in this.cargo_manifest)
        {
            if (val.Key == key)
            {
                int inner_index = 0;
                foreach (var nv in this.cargo_manifest)
                {
                    if (inner_index == index)
                    {
                        temp_dic.Add(nv.Key, new_value);
                    }

                    else
                    {
                        temp_dic.Add(nv.Key, nv.Value);
                    }

                    ++inner_index;
                }
                this.cargo_manifest = temp_dic;
                return true;
            }
            ++index;
        }
        return false;

    }

    public Dictionary<string, int> getCargoManifest()
    {
        if (this.cargo_manifest == null)
        {
            throw new Exception("Cargo manifest hasn't been initialized yet. Early call.");
        }

        else
            return this.cargo_manifest; 
    }





    //Is capacity valid?
    private bool checkCapacity()
    {
        if (this.actual_capacity < 0)
        {
            Console.WriteLine("Capacity is a negative number. Error.");
            Console.Read();
            return false;
        }

        else return true;
    }

    private void initCargoManifest()
    {
        var goods_table = MapBuilder.getMapList()[0].getResourcesTable();
        this.cargo_manifest = new Dictionary<string, int>(); 
        foreach(var entry in goods_table)
        {
            this.cargo_manifest.Add(entry.Key, 0);
        }

        //Check correct cargo initialization.
        foreach (var entry in this.cargo_manifest)
        {
            Console.WriteLine("Key: " + entry.Key + " Value: " + entry.Value);
        }
        this.manifest_initialized = true;
    }

}