﻿using System;
using System.Collections.Generic;

public enum Ship_Type
{
    UNDEFINED = 0,
    SMALL_SHIP, 
};



static public class ShipBuilder
{
    //Lista interna para identificar a las naves.
    static private Dictionary<string, bool> ships_id_list = new Dictionary<string, bool>(); 

    static public StandardShip createShip(Ship_Type ship_type)
    {
        switch (ship_type)
        {
            case Ship_Type.SMALL_SHIP:
                Console.WriteLine("Creating small ship...");
                Console.WriteLine("");
                StandardShip ship = new StandardShip();
                return ship;
            default:
                Console.WriteLine("Unknown option. Null will be returned.");
                return null;
        }
    }

    public abstract class Ship : Transactor
    {
        Ship_Type ship_type = Ship_Type.SMALL_SHIP;

        public Cargo cargo;

        protected int jump_capacity;
        protected int jumps_remaining;
        private int location_index = 0;
        private int money = 0;

        private string id;

        private Dictionary<string, Transaction> transaction_list;

        public abstract bool jump();

        public abstract void printAllProperties();
        protected abstract void setJumpCapacity(int jump_capacity);




        /*|Implementacion de las funciones de las interfaces*|
        /******************************************************************************************************/

        void Transactor.setTransactionList()
        {
            this.transaction_list = new Dictionary<string, Transaction>();
        }

        //Add transaction to actual transactor list an optionally add it to the global transaction list.
        void Transactor.addTransactionToHistory(Transaction t, bool add_to_glob)
        {
            //Falta especificar como construir la transaccion.
            string t_id = "Transaction: " + MapBuilder.getTransactionsGlobList().Count.ToString();
            this.transaction_list.Add(t_id, t);
            KeyValuePair<string, Transaction> transaction = new KeyValuePair<string, Transaction>(t_id, t);
            if (add_to_glob)
                MapBuilder.addTransactionToGlobList(transaction);
        }


        bool Transactor.isBuyer()
        {
            Console.WriteLine("Ship doesn't buy resources with money");
            return false;
        }

        bool Transactor.isSeller()
        {
            Console.WriteLine("Ship sells resources for money");
            return true;
        }

        bool Transactor.isBuyerAndSeller()
        {
            Console.WriteLine("Ship doesn't buy resources and also sells them.");
            return false;
        }
        /******************************************************************************************************/



        protected Ship()
        {
            this.id = "Ship " + ships_id_list.Count;
            ships_id_list.Add(this.id, true);
        }

        public int getJumpCapacity()
        {
            return this.jump_capacity;
        }

        public int getJumpsRemaining()
        {
            return this.jumps_remaining;
        }

        public int getMoneyRemaining()
        {
            return this.money; 
        }

        //Set the ship position passing its index in the planet array.
        public void setLocation (int location_planet)
        {
            var loc = MapBuilder.getMapList()[0].getPlanets()[location_planet].getPlanetId();
            Console.WriteLine("New location is: " + loc);
            this.location_index = location_planet;             
        }

        public void setMoney(int money)
        {
            this.money = money; 
        }

        public void addMoney(int money_to_add)
        {
            this.money += money_to_add; 
        }

        public string getId()
        {
            return this.id;
        }

        public Ship_Type getShipType()
        {
            return this.ship_type;
        }

        public PlanetBuilder.Planet getLocation()
        {
            return MapBuilder.getMapList()[0].getPlanets()[this.location_index];
        }

        //Asumimos que la nave vendera en la ubicacion en la que esté, y que solo podra saber cuanto pagan por
        //el material en cuestion cuando este en el sitio en el que quiera venderlo.
        public Transaction precalcResourceSell(string resource_id, int cuantity)
        {
            var planet_buyer = this.getLocation();
            
            //El recurso existe?
            if (planet_buyer.getResourcesMultTable().ContainsKey(resource_id))
            {
                
                int base_value = this.getLocation().getHomeMap().getResourcesTable()[resource_id];
                Console.WriteLine("Base value obtained for: " + resource_id + " is: " + base_value);

                int planet_multiplier = this.getLocation().getResourcesMultTable()[resource_id];

                int final_value = base_value * planet_multiplier * cuantity;
                Console.WriteLine("Going to obtain: " + final_value + " for selling: " + cuantity + " units of: " + resource_id +
                    " at planet " + this.getLocation().getPlanetId());
                Console.WriteLine("Actual amount of: " + resource_id + " " + this.cargo.getCargoManifest()[resource_id] + " units");

                int remaining_amount = this.cargo.getCargoManifest()[resource_id] - cuantity;
                Console.WriteLine("Remaining amount of resource: " + resource_id + " after transaction: " + remaining_amount + " units.");

                if (remaining_amount < 0)
                {
                    Console.WriteLine("Cant sell " + cuantity + " units of " + resource_id + ". Not enough amount of said resource.");
                    return TransactionBuilder.buildFailedTransaction();

                }

                else
                {
                    Transaction t = new Transaction(true, this.getId(), this.getLocation().getPlanetId(),
                        resource_id, final_value, cuantity);
                    t.setSuccess(true);
                    return t;
                }
                //Hacemos la transaccion:
                //  * Substraemos la cantidad de recursos que hayamos querido vender.
                //  * Añadimos la cantidad de dinero en funcion de cuanto hayamos vendido


            }

            else
            {
                Console.WriteLine("Resource id" + resource_id + "doesn't exist at this map.");
                return TransactionBuilder.buildFailedTransaction();
            }
        }


        public Transaction sellResource(string resource_id, int cuantity)
        {
            var planet_buyer = this.getLocation();

            //El recurso existe?
            if (planet_buyer.getResourcesMultTable().ContainsKey(resource_id))
            {

                int base_value = this.getLocation().getHomeMap().getResourcesTable()[resource_id];
                Console.WriteLine("Base value obtained for: " + resource_id + " is: " + base_value);

                int planet_multiplier = this.getLocation().getResourcesMultTable()[resource_id];

                int final_value = base_value * planet_multiplier * cuantity;
                Console.WriteLine("Going to obtain: " + final_value + " for selling: " + cuantity + " units of: " + resource_id +
                    " at planet " + this.getLocation().getPlanetId());
                Console.WriteLine("Actual amount of: " + resource_id + " " + this.cargo.getCargoManifest()[resource_id] + " units");



                int remaining_amount = this.cargo.getCargoManifest()[resource_id] - cuantity;
                Console.WriteLine("Remaining amount of resource: " + resource_id + " after transaction: " + remaining_amount + " units.");

                if (remaining_amount < 0)
                {
                    Console.WriteLine("Cant sell " + cuantity + " units of " + resource_id + ". Not enough amount of said resource.");
                    return TransactionBuilder.buildFailedTransaction();

                }

                else
                {
                    Transaction t = new Transaction(true, this.getId(), this.getLocation().getPlanetId(),
                        resource_id, final_value, cuantity);
                    this.addMoney(final_value);
                    this.cargo.substractManifestAmount(resource_id, cuantity);
                    t.setSuccess(true);
                    MapBuilder.addTransactionToGlobList(t);
                    return t;
                }
              

            }

            else
            {
                Console.WriteLine("Resource id" + resource_id + "doesn't exist at this map.");
                return TransactionBuilder.buildFailedTransaction();
            }
        }
       
    }

    public class StandardShip : Ship
    {

        private bool valid_instance = false;
        private bool out_of_jumps = false;


        public StandardShip(int jump_capacity = 10, int cargo = 100) : base()
        {
            this.setJumpCapacity(10);
            this.cargo = new Cargo(1000);
            this.checkAllFields();
        }

        protected override void setJumpCapacity(int jump_capacity)
        {
            this.jump_capacity = jump_capacity;
            this.jumps_remaining = jump_capacity;
        }

        public override void printAllProperties()
        {
            Console.WriteLine("Ship cargo: ");
            Console.WriteLine(" " + "Overall Capacity: " + this.cargo.getTotalCapacity());
            Console.WriteLine(" " + "Actual Capacity: " + this.cargo.getTotalCapacity());
            Console.WriteLine("");
            Console.WriteLine("Ship stats: ");
            Console.WriteLine(" " + "Jumps: " + this.getJumpCapacity());
            Console.WriteLine(" " + "Jumps remaining: " + this.getJumpsRemaining());

        }

        //ESTA A MEDIAS, NO IMPLEMENTADA
        //Los planetas pequeños solo pueden saltar una vez, de planeta en planeta.
        //Realmente la nave no se mueve, solo comprueba que pueda moverse y guarda sus opciones temporalmente.
        override public bool jump()
        {
            if (!this.checkRemainingJumps())
            {
                Console.WriteLine("Can't make any more jumps.");
                return false;
            }
            else
            {
                Console.WriteLine("Space ship is located at: " + this.getLocation().getPlanetId() + " and it can jump to: ");

                foreach (var planet in this.getAvailableDestinations())
                {
                    Console.WriteLine("     Planet: " + planet.getPlanetId());
                }

                var options = this.getAvailableDestinations();

                Console.WriteLine("Press the number corresponding to an option: ");
                int c = 0;
                List<int> stored_opts = new List<int>(options.Count);
                foreach(var o in options)
                {
                    stored_opts.Add(c);
                    Console.WriteLine("Option1: " + c);
                    ++c;
                }
                
                int chosen = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Number passed: " + chosen);

                if (chosen < 0 || chosen > stored_opts.Count)
                {
                    do
                    {
                        Console.WriteLine("Invalid option! Introduce an option between 0 and " + stored_opts.Count);
                        chosen = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Number passed: " + chosen);
                        Console.WriteLine("Max: " + stored_opts.Count);
                    } while (chosen < 0 || chosen > stored_opts.Count);
                }
               

              
                Console.WriteLine("Jump will be done:");
                Console.WriteLine("     Actual location: " + this.getLocation().getPlanetId());
                var dest = options[chosen]; 
                Console.WriteLine("     Target location: " + dest.getPlanetId());
                this.setLocation(dest.getPlanetIndex());
                Console.WriteLine();
                Console.WriteLine("Jump done! Actual location is: " + this.getLocation().getPlanetId());
                Console.WriteLine();
                Console.Read();

                this.jump_capacity--;
                return true;
            }
            
        }

        private List<PlanetBuilder.Planet> getAvailableDestinations()
        {
            List<PlanetBuilder.Planet> planets = new List<PlanetBuilder.Planet>();
            MapBuilder.getMapList()[0].getPlanets();
            var actual_location = this.getLocation();
           
            foreach (var route in actual_location.getRoutes())
            {
                //Console.WriteLine("");
                //Console.WriteLine("************************************************************");
                //Console.WriteLine("ROUTE: " + route.getRouteId());
                string prev = route.getPreviousPlanet().getPlanetId();
                string next = route.getNextPlanet().getPlanetId();
                //Console.WriteLine("     --> Planet previous: " + route.getPreviousPlanet().getPlanetId());
                //Console.WriteLine("     --> Planet next: " + route.getNextPlanet().getPlanetId());

                if (actual_location.getPlanetId() != prev)
                {
                    //Console.WriteLine("This: " + actual_location.getPlanetId() + " doesnt equals to: " + prev);
                    planets.Add(route.getPreviousPlanet());
                }

                if (actual_location.getPlanetId() != next)
                {
                    //Console.WriteLine("This: " + actual_location.getPlanetId() + " doesnt equals to: " + next);
                    planets.Add(route.getNextPlanet());
                }
                //Console.WriteLine("************************************************************");
                //Console.WriteLine("");

            }
            return planets;
        }

        private bool checkAllFields()
        {
            if (this.cargo == null)
            {
                Console.WriteLine("Invalid instance. Cargo instance returns null.");
                Console.Read();
                return false;
            }
            else return true;
        }

        //Can the ship keep jumping?
        private bool checkRemainingJumps()
        {
            if (this.jumps_remaining <= 0)
            {
                Console.WriteLine("Ship out of jumps, jump will not be done.");
                return false;
            }
            else if (this.jumps_remaining < 0)
            {
                Console.WriteLine("Ship out of jumps, jump will not be done.");
                return false;
            }
            else
                return true;
        }
    }

}