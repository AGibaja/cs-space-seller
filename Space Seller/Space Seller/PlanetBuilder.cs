﻿using System;
using System.Collections.Generic;



static public class PlanetBuilder
{
    static public Planet buildPlanet(string planet_name = "Unnamed Planet")
    {
        Planet planet = new Planet(planet_name);
        return planet;
    }

    public class Planet : Transactor
    {
        private string planet_id = "Unnamed Planet";
        //Lista de compradores que han comprado en este planeta. Solo puede haber comprado una vez.
        //Esta tabla guarda los compradores y la cantidad de recursos que han aportado.

        private int planet_index;

        public bool has_routes = false;

        //Lista de rutas que pasan por este planeta.
        private List<RouteBuilder.Route> routes = null;

        List<int> resources_multipliers = null;

        private Dictionary<string, int> buyers_table = null;
        private Dictionary<string, Transaction> transaction_list; 

        private int map_owner_index = 0;

       
        /*|Implementacion de las funciones de las interfaces*|
        /******************************************************************************************************/
        void Transactor.setTransactionList()
        {
            this.transaction_list = new Dictionary<string, Transaction>();
        }

        //Add transaction to actual transactor list an optionally add it to the global transaction list.
        void Transactor.addTransactionToHistory(Transaction t, bool add_to_glob)
        {
            //Falta especificar como construir la transaccion.
            string t_id ="Transaction: " +  MapBuilder.getTransactionsGlobList().Count.ToString();
            this.transaction_list.Add(t_id, t);
            KeyValuePair<string, Transaction> transaction = new KeyValuePair<string, Transaction>(t_id, t);
            if (add_to_glob)
                MapBuilder.addTransactionToGlobList(transaction);
        }

        bool Transactor.isBuyer()
        {
            Console.WriteLine("Planet buys resources with money");
            return true;
        }

        bool Transactor.isSeller()
        {
            Console.WriteLine("Planet sells resources for money");
            return false;
        }

        bool Transactor.isBuyerAndSeller()
        {
            Console.WriteLine("Planet doesn't buy resources and sells them.");
            return false;
        }
        /******************************************************************************************************/



        public Planet(string id = "Unnamed Planet")
        {
            this.planet_id = id;
            this.routes = new List<RouteBuilder.Route>();
            this.setBuyersTable(); 
        }

        //Como los diferentes planetas pagan cantidades distintas cantidades por distinos recursos, se establecen multiplicadores por planeta.
        public void setResourcesMultipliers()
        {
            this.resources_multipliers = new List<int>();
            this.resources_multipliers.Capacity = this.getHomeMap().getResourcesTableSize();

            Random rnd = new Random();

            for (int i = 0; i < this.resources_multipliers.Capacity; ++i)
            {
                int number = rnd.Next(0, 10);
                this.resources_multipliers.Add(number);
            }
        }

        public void setPlanetId(string planet_id)
        {
            this.planet_id = planet_id;
        }

        public void addRoute(RouteBuilder.Route route)
        {
            this.routes.Add(route);
            this.has_routes = true;
        }

        public void setPlanetIndex(int index)
        {
            this.planet_index = index;
        }

        public int getPlanetIndex()
        {
            return this.planet_index;
        }

        public string getPlanetId()
        {
            return this.planet_id;
        }

        public List<int> getResourcesMultipliers()
        {
            return this.resources_multipliers;
        }

        public List<RouteBuilder.Route> getRoutes()
        {
            if (this.routes.Capacity == 0)
            {
                Console.WriteLine("Empty vector returned.");
                Console.Read();
                return null; 
            }
            else
                return this.routes; 
        }
        
        public Dictionary<string, int> getBuyersList()
        {
            if (this.buyers_table == null)
                throw new Exception("Buyers table not initialized.");
            else if (this.buyers_table.Count == 0)
            {
                //Debug.Log("Empty buyers list.");
                Console.WriteLine("Empty buyers table.");
                return this.buyers_table;
            }
            else
                return this.buyers_table; 
        }

        //Retorna el mapa al que pertenece el planeta.
        public MapBuilder.Map getHomeMap()
        {
            var a = MapBuilder.getMapList()[this.map_owner_index];
            return a ;
        }

        private void setBuyersTable()
        {
            this.buyers_table = new Dictionary<string, int>(); 
        }

        private void addBuyer(string buyer_id, int resources_spent)
        {
            if (this.buyers_table == null)
                throw new Exception("Buyers table not initialized.");
            else
            {
                this.buyers_table.Add(buyer_id, resources_spent);
            }
        }

        private bool canShipBuy(string id)
        {
            if (this.buyers_table.ContainsKey(id))
            {
                //Debug.Log("Can't buy!");
                Console.WriteLine("Can't buy!");
                return false;
            }
            else
            {
                //Debug.Log("Can buy :)");
                return true;

            }
        }

        //Retorna los multiplicadores como un diccionario.
        //Plantearse ocupar mas memoria para que sea mas rapido en vez de computar cada vez que se ejecute.
        public Dictionary<string, int> getResourcesMultTable()
        {
            Dictionary<string, int> resources_table = new Dictionary<string, int>();
            int i = 0; 
            foreach (var entry in this.getHomeMap().getResourcesTable())
            {
                resources_table.Add(entry.Key, this.getResourcesMultipliers()[i]);
                ++i;
            }

            return resources_table;
        }

        public KeyValuePair<string, int> getEntryById(string id)
        {
            KeyValuePair<string, int> kp;
            if (this.getResourcesMultTable().ContainsKey(id))
            {
                kp = new KeyValuePair<string, int>(id,
                    this.getResourcesMultTable()[id]);
                return kp;
            }
            else
            {
                
                kp = new KeyValuePair<string, int>(null, -1);
                return kp;

            }
        }
    }
}
