﻿public interface Transactor
{

    void setTransactionList();
    void addTransactionToHistory(Transaction t, bool add_to_glob = true);



    //Si da/paga dinero
    bool isBuyer();   
    //Si vende otra cosa y a cambio recibe dinero
    bool isSeller();
    //Si hace las las dos mencionadas arriba.
    bool isBuyerAndSeller();
    
}

public static class TransactionBuilder
{
    public static Transaction buildFailedTransaction()
    {
        Transaction failed_t = new Transaction();
        failed_t.setSuccess(false);
        return failed_t; 
    }
}

public class Transaction
{



    public struct Result
    {
        public bool is_virtual; 
        public bool success;
        public string buyer_id;
        public string seller_id;
        public string resource_id;        
        public int money_acquired;
        public int quantity_sold;

    };

    public Result result;

    public Transaction()
    {

    }

    

    public Transaction(bool is_virtual = false, string buyer_id = null
        , string seller_id = null, string resource_id = null)
    {
        this.result = new Result();
    }

    public Transaction(bool is_virtual = false, string buyer_id = null
        , string seller_id = null, string resource_id = null, int money_acquired = 0,
        int quantity_sold = 0)
    {
        this.result = new Result();
    }

   

    public void setBuyerId(string buyer_id)
    {
        this.result.buyer_id = buyer_id;
    }

    public void setSellerId(string seller_id)
    {
        this.result.seller_id = seller_id;
    }

    public void setResourceId(string id)
    {
        this.result.buyer_id = id;
    }

    public void setMoneyAcquired(int money_acquired)
    {
        this.result.money_acquired = money_acquired;
    }

    public void setQuantitySold(int quantity_sold)
    {
        this.result.quantity_sold = quantity_sold; 
    }
    
    public void setSuccess(bool success)
    {
        this.result.success = true;
    }

    public Result getTransactionResult()
    {
        return this.result; 
    }



}