﻿using System;
using System.Collections.Generic;



public static class RouteBuilder
{

    public static Route buildRoute(PlanetBuilder.Planet previous_planet, PlanetBuilder.Planet next_planet, string id = "Unnamed Route.")
    {

        Route r = new Route(previous_planet, next_planet, id);
        return r; 
    }

   
    public class Route
    {
        private PlanetBuilder.Planet previous_planet = null;
        private PlanetBuilder.Planet next_planet = null;
        private string id = "Unnamed Route.";


        public Route(PlanetBuilder.Planet previous_planet = null, PlanetBuilder.Planet next_planet = null, string id = "Unnamed Route.")
        {
            this.previous_planet = previous_planet;
            previous_planet.addRoute(this);
            this.next_planet = next_planet;
            next_planet.addRoute(this);
            this.checkValidRoute();
            this.setRouteId(generateName());
        }

        public void setRouteId(string id)
        {
            this.id = id; 
        }

        public string generateName()
        {
            string generated_name;
            generated_name = "ROUTE " + previous_planet.getPlanetId() + next_planet.getPlanetId();
            return generated_name;
        }

        public void printPlanetsConnected()
        {
            Console.WriteLine("Printing planets connected by route: " + this.getRouteId());
            Console.WriteLine(this.next_planet.getPlanetId());
            Console.WriteLine(this.previous_planet.getPlanetId());
        }

        public string getRouteId()
        {
            return this.id; 
        }

        public PlanetBuilder.Planet getPreviousPlanet()
        {
            return this.previous_planet;
        }

        public PlanetBuilder.Planet getNextPlanet()
        {
            return this.next_planet;
        }

        private bool checkValidRoute()
        {
            if (this.next_planet == null)
            {
                Console.WriteLine("Next planet is null.");
                Console.Read();
                return false; 
            }

            else if (this.previous_planet == null)
            {
                Console.WriteLine("Previous planet is null.");
                Console.Read(); 
                return false;
            }

            else
                return true; 
        }

    }

}