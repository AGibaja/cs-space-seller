﻿using System;
using System.Collections.Generic; 

public static class ResourceBuilder
{
    public static List<Resource> resources_list = new List<Resource>();
    public static bool resources_created = false;

    //Not very useful...
    public static Resource buildResource(string id = "Unnamed Resource.", int base_value = 1)
    {
        Resource r = new Resource(id, base_value);
        return r;
    }


    public static Dictionary<string, int> buildResourcesTables()
    {
        var resources_table = new Dictionary<string, int>();
        resources_table.Add("Energy", 2);
        resources_table.Add("Food", 1);
        resources_table.Add("Fuel", 4);
        buildResourcesList(resources_table); 
        resources_created = true;
        return resources_table;
    }


    public static void buildResourcesList(Dictionary<string, int> resources_table)
    {
        List<Resource> resources_list_ = new List<Resource>(); 
        foreach (KeyValuePair<string, int> kv in resources_table)
        {
            Resource r = new Resource(kv);
            resources_list_.Add(r); 
        }
        resources_list = resources_list_; 
    }


    public static List<Resource> getResourcesList()
    {
        return resources_list; 
    }


    public class Resource
    {
        private string resource_id = "Unnamed Resource.";

        private int base_value = 1;

        public Resource(string id = "Unnamed Resource", int base_value = 1)
        {
            this.base_value = base_value;
            this.resource_id = id;
        }

        public Resource(KeyValuePair<string, int> resource_entry)
        {
            this.resource_id = resource_entry.Key;
            this.base_value = resource_entry.Value;
        }

        public void setId(string id)
        {
            this.resource_id = id; 
        }

        public void setBaseValue(int base_value)
        {
            this.base_value = base_value; 
        }

        public string getId()
        {
            return this.resource_id; 
        }

        public int getBaseValue()
        {
            return this.base_value; 
        }
    }
    
    
}